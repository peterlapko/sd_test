<?php

namespace Sd\Test\Controller\Index;

use Magento\Framework\App\Action\Context;
use Sd\Test\Model\ContactFactory;
use Sd\Test\Model\ResourceModel\Contact as ContactResource;

class Post extends \Magento\Framework\App\Action\Action
{
    protected $contactFactory;
    protected $contactResource;

    public function __construct(
        Context $context,
        ContactFactory $contactFactory,
        ContactResource $contactResource
    ) {
        $this->contactFactory = $contactFactory;
        $this->contactResource = $contactResource;
        parent::__construct($context);
    }

    public function execute()
    {
        $post = $this->getRequest()->getPostValue();
        if (!$post) {
            $this->_redirect('*/*/');
            return;
        }

        try {

            $error = false;

            if (!\Zend_Validate::is(trim($post['name']), 'NotEmpty')) {
                $error = true;
            }
            if (!\Zend_Validate::is(trim($post['comment']), 'NotEmpty')) {
                $error = true;
            }
            if (!\Zend_Validate::is(trim($post['email']), 'EmailAddress')) {
                $error = true;
            }
            if (\Zend_Validate::is(trim($post['hideit']), 'NotEmpty')) {
                $error = true;
            }
            if ($error) {
                throw new \Exception();
            }

            $contactModel = $this->contactFactory->create();
            $contactModel->setData($post);
            $this->contactResource->save($contactModel);

            $this->messageManager->addSuccess(
                __('Thanks for contacting us with your comments and questions.')
            );
            $this->_redirect('sdtest/contact/index');
            return;
        } catch (\Exception $e) {
            $this->messageManager->addError(
                __('We can\'t process your request right now.')
            );
            $this->_redirect('sdtest/contact/index');
            return;
        }
    }
}
