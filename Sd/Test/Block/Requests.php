<?php

namespace Sd\Test\Block;

use \Magento\Framework\View\Element\Template\Context;
use \Sd\Test\Model\ResourceModel\Contact\CollectionFactory;


class Requests extends \Magento\Framework\View\Element\Template
{
    protected $collectionFactory;
    /**
     * @param Context $context
     * @param array $data
     */
    public function __construct(
        Context $context,
        CollectionFactory $collectionFactory,
        array $data = []
    ) {
        $this->collectionFactory = $collectionFactory;
        parent::__construct($context, $data);
    }

    public function getCollection()
    {
        return $this->collectionFactory->create();
    }
}
