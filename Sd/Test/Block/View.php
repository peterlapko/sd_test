<?php

namespace Sd\Test\Block;

use \Magento\Framework\View\Element\Template\Context;

class View extends \Magento\Framework\View\Element\Template
{
    /**
     * @param Context $context
     * @param array $data
     */
    public function __construct(Context $context, array $data = [])
    {
        parent::__construct($context, $data);
        $this->_isScopePrivate = true;
    }

    public function getFormAction()
    {
        return $this->getUrl('sdtest/contact/post', ['_secure' => true]);
    }
}
